import discord
from discord.ext import commands
import sys
import os
import json
import inspect
import traceback
import shelve
from time import time

class UnDict:
    def __init__(self, d):
        self.__dict__.update(d)

def load_files():
    global cred, conf, msgs, _owners
    with open('credentials.json') as f: cred = UnDict(json.load(f))
    with open('config.json') as f:      conf = UnDict(json.load(f))
    with open('messages.json') as f:    msgs = UnDict(json.load(f))
    _owners = cred.owners
    del cred.__dict__['owners']

load_files()

bot = commands.Bot(command_prefix=conf.prefix, description="A small bot that gives out daily points and shows people on leaderboards.\nCreated by hmry (https://hmry.io) for potatonoodles.\nSource: https://gitlab.com/hmry/something-random-bot")

class UserData:
    slots = ['points', 'last']

    def __init__(self):
        self.points = conf.points
        self.last = time()

    def update(self):
        self.points += conf.points
        self.last = time()

    def __repr__(self):
        return '<UserData points:{} last:{}>'.format(self.points, self.last)

def open_shelf():
    global shelf, local_lb, global_lb
    shelf = shelve.open(conf.shelf_path, protocol=4, writeback=True)

    if 'local_lb' not in shelf: shelf['local_lb'] = {}
    if 'global_lb' not in shelf: shelf['global_lb'] = []

    local_lb = shelf['local_lb']
    global_lb = shelf['global_lb']

open_shelf()

user_cache = {}
async def get_user(uid):
    if uid in user_cache:
        return user_cache[uid]
    else:
        u = await bot.get_user_info(uid)
        user_cache[uid] = u
        return u

async def fetch_user(uid):
    if uid not in user_cache:
        user_cache[uid] = await bot.get_user_info(uid)

def update_leaderboards(ui, ci, points):
    if ci not in local_lb:
        local_lb[ci] = [ui]

    else:
        channel_lb = local_lb[ci]
        if ui in channel_lb:
            channel_lb.sort(key=lambda x: shelf[x][ci].points, reverse=True)
        else:
            if len(channel_lb) < conf.local_leaderboard_size:
                channel_lb.append(ui)
                channel_lb.sort(key=lambda x: shelf[x][ci].points, reverse=True)
            elif points > shelf[channel_lb[-1]][ci].points:
                channel_lb[-1] = ui
                channel_lb.sort(key=lambda x: shelf[x][ci].points, reverse=True)

    if ui in global_lb:
        global_lb.sort(key=lambda x: shelf[x]['global'], reverse=True)
    else:
        if len(global_lb) < conf.global_leaderboard_size:
            global_lb.append(ui)
            global_lb.sort(key=lambda x: shelf[x]['global'], reverse=True)
        elif points > shelf[global_lb[-1]][ci].points:
            global_lb[-1] = ui
            global_lb.sort(key=lambda x: shelf[x]['global'], reverse=True)


def is_superuser(ctx):
    return ctx.message.author.id in cred.superusers

def is_owner(ctx):
    return ctx.message.author.id in _owners

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

@commands.check(is_owner)
@bot.command(name='eval', pass_context=True, hidden=True)
async def eval_command(ctx, *, code: str):
    env = {
        'b': bot,
        'ctx': ctx,
        'm': ctx.message,
        's': ctx.message.server,
        'c': ctx.message.channel,
        'a': ctx.message.author,
        'd': discord
    }
    env.update(globals())

    code = code.strip('` ')
    result = None

    try:
        result = eval(code, env)
        if inspect.isawaitable(result):
            result = await result
        await bot.say('```py\n' + str(result) + '```')
    except Exception:
        await bot.say('```py\n' + traceback.format_exc() + '```')

@bot.command(pass_context=True)
async def balance(ctx):
    """Shows your point balance."""
    channel_id = ctx.message.channel.id
    user_id = ctx.message.author.id

    if user_id in shelf and channel_id in shelf[user_id]:
        await bot.say(msgs.balance.format(ctx=ctx, usrd=shelf[user_id][channel_id], usrg=shelf[user_id]['global']))
    else:
        await bot.say(msgs.balance_no_points.format(ctx=ctx))

@bot.command(pass_context=True)
async def daily(ctx):
    """Redeem your daily points."""
    channel_id = ctx.message.channel.id
    user_id = ctx.message.author.id

    success = True

    if user_id not in shelf:
        shelf[user_id] = {channel_id: UserData(), 'global': conf.points}

        await bot.say(msgs.redeem_first_points.format(ctx=ctx, usrd=shelf[user_id][channel_id], usrg=shelf[user_id]['global']))


    elif channel_id not in shelf[user_id]:
        shelf[user_id][channel_id] = UserData()
        shelf[user_id]['global']  += conf.points

        await bot.say(msgs.redeem_first_points.format(ctx=ctx, usrd=shelf[user_id][channel_id], usrg=shelf[user_id]['global']))
        update_leaderboards(user_id, channel_id, shelf[user_id][channel_id].points)

    else:
        if time() - shelf[user_id][channel_id].last > conf.cooldown:
            shelf[user_id][channel_id].update()
            shelf[user_id]['global'] += conf.points

            await bot.say(msgs.redeem.format(ctx=ctx, usrd=shelf[user_id][channel_id], usrg=shelf[user_id]['global']))
            update_leaderboards(user_id, channel_id, shelf[user_id][channel_id].points)

        else:
            await bot.say(msgs.redeem_cooldown.format(ctx=ctx, usrd=shelf[user_id][channel_id], usrg=shelf[user_id]['global']))
            success = False

    if success:
        update_leaderboards(user_id, channel_id, conf.points)
        shelf.sync()


@bot.command(pass_context=True)
async def leaderboards(ctx):
    """Displays the global and local leaderboards."""
    cid = ctx.message.channel.id

    for uid in global_lb:
        await fetch_user(uid)

    for uid in local_lb.get(cid, []):
        await fetch_user(uid)


    await bot.say('{}\n{}\n\n{}\n{}'.format(
        msgs.leaderbord_global_header.format(ctx=ctx),
        '\n'.join(msgs.leaderbord_global_row.format(ctx=ctx, nr=i + 1, usr=user_cache[uid], points=shelf[uid]['global']) for i, uid in enumerate(global_lb)),
        msgs.leaderbord_local_header.format(ctx=ctx),
        '\n'.join(msgs.leaderbord_local_row.format(ctx=ctx, nr=i + 1, usr=user_cache[uid], points=shelf[uid][cid].points) for i, uid in enumerate(local_lb.get(cid, [])))
    ))

@commands.check(is_superuser)
@bot.command(name='reload', pass_context=True, hidden=True)
async def reload_command(ctx):
    await bot.say(msgs.reload.format(ctx=ctx))
    try:
        load_files()
    except Exception:
        await bot.say(msgs.reload_error.format(ctx=ctx) + '\n```py\n' + traceback.format_exc() + '```')

@commands.check(is_superuser)
@bot.command(name='shelf', pass_context=True, hidden=True)
async def shelf_command(ctx):
    await bot.say('```py\n' + str(dict(shelf)) + '```')

@commands.check(is_superuser)
@bot.command(pass_context=True, hidden=True)
async def wipe(ctx):
    await bot.say(msgs.wipe_ask.format(ctx=ctx, msgs=msgs))
    reply = await bot.wait_for_message(author=ctx.message.author, channel=ctx.message.channel)

    if reply.content != msgs.wipe_phrase:
        await bot.say(msgs.wipe_cancled.format(ctx=ctx))
    else:
        await bot.say(msgs.wipe_confirmed.format(ctx=ctx))
        global shelf
        shelf.close()
        os.remove(conf.shelf_path)
        open_shelf()

@commands.check(is_owner)
@bot.command(pass_context=True, hidden=True)
async def shutdown(ctx):
    await bot.say(msgs.shutdown.format(ctx=ctx))
    shelf.close()
    sys.exit()

bot.run(cred.token)
